#include<stdio.h>
#include <stdbool.h>

////////////////////////////////////////////////////////////////

void red () {
  printf("\033[1;31m");
}

void magenta () {
  printf("\033[0;35m");
}

void blue () {
  printf("\033[;34m");
}

void green () {
  printf("\033[1;32m");
}

void yellow () {
  printf("\033[;33m");
}

void yellowbold () {
  printf("\033[01;33m");
}

void reset () {
  printf("\033[;0m");
}

//////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

/* double novamente()  {
{char resposta;
do {
  yellow();
   printf("\nTentar novamente?  (S/N)\n");
   scanf(" %c", &resposta);
   getchar();
switch(resposta)
     {
         case 's': 
         case 'S':
         return ();
         break;
         case 'n':
         case 'N':     
         printf("\nAté logo!\n\n\033[;34mby: @hackbartjoao\n\033[;33m");
         return 0;
         break;
         default:
           green();
           printf("(S)im ou (N)ão");
     }
    }while(1);}
} */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void fibonacci () {

 int nmr, sqnc;
  char resposta;

  printf("\033[;33m\nVerificador do 'N' termo da sére de Fibonacci.\n\033[;33m\033[0;35m\n");
  getchar();


  magenta();
  printf("\nIntroduzir 'N':\n");
  scanf("%d", &nmr);
  if (nmr<=0)
  printf("\033[;33m\033[1;31m\nNúmero deve ser positivo!");
  if ((nmr>0) && (nmr<=2)) {
  printf("\033[;33m\033[0;35m\nO número de fibonacci para 'N=\033[0;36m%d\033[0;35m' é \033[0;36m1\033[0;35m!", nmr);
  }
  if (nmr>=3) {
  sqnc=(nmr-2)+(nmr-1);
  printf("\033[;33m\033[0;35m\nO número de fibonacci para 'N=\033[0;36m%d\033[0;35m' é \033[0;36m%d\033[0;35m!", nmr, sqnc);
  } 
}

/////////////////////////////////////////////////////////////////

void pesoideal () {

  char resposta, gender;
  float height, male, female;

   printf("\033[;33m\nVerificador do peso ideal\n\033[0;35m\n");
  getchar();

    printf("\nIntroduzir Altura (Ex: 1.69)\n");
    scanf("%f", &height);
    getchar();
    
    printf("\nIntroduzir Sexo [H/M]\n");
    scanf("%c",&gender);
     switch(gender)
    {
        case 'H':
        case 'h':
            male=72.7*height-58;
            printf("Seu peso ideal é \033[0;36m%.f\033[0;35m Kg!", male);
            break;
        case 'M':
        case 'm':
            female=62.1*height-44.7; 
            printf("Seu peso ideal é \033[0;36m%.f\033[0;35m Kg!", female);
            break;
        default:
            green();
            printf("\nDigite [H]omem ou [M]ulher!\n");
    }
    printf("\n");
}

//////////////////////////////////////////////////////////////////

void anobissexto() {

  int ano;
  char resposta; 

     printf("\033[;33m\nVerificador do ano Bisexto.\033[0;35m\n");
  getchar();
do {
        magenta ();
        printf("\nIntroduz o Ano: \n");
        scanf ("%d", &ano);
        if((ano>0) && (ano>=1800)) 
 		    break;
        red ();
        printf("Digite um número positivo, maior que 1800.\nEx: 2020, 2001, 1998.\n");
    }while (1);
      
    if ((ano%4 == 0) && (ano%100 != 0)) {
    green();
    printf("\033[1;32m\nAno %d é Bissexto \n", ano);
 }else {
    red();
    printf("\033[1;31m\nAno %d não é Bissexto \n", ano);	
}   
}

///////////////////////////////////////////////////////
void anosdiasnumero() {

 int ano, mes, dias;
  printf("\n\nVerificador do Nmr. de dias.");
  magenta();
  printf("\n\nIntroduzir ANO: ");
  scanf("%d", &ano);
  printf("Introduzir MES: ");
  scanf("%d", &mes);

  if(mes==2)
  {
      if((ano%400==0) || ((ano%4==0) && (ano%100!=0))) {
        printf("29");
      } else
        printf("28");
  } else if(mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12)
  {
    printf("31");
  } else
  {
    printf("30");
  }
}
/////////

bool retry()  {
char resposta;
//float one=('m1');
//float two=('m2');
//float three=('m3');

do {
  yellow();
   printf("\n\n\n--------------------------\n");
   //printf("[1] Fibonacci\n");
   //printf("[2] Peso Ideal\n");
   //printf("[3] Ano Bissexto\n");
   printf("\033[1;32m[M]enu ou [S]AIR?\n");
   yellow();
   scanf(" %c.", &resposta);
   getchar();
switch(resposta)
     {
         //case '1':
         //printf("1");
         //return (main()); 
         //break;
         //case '2':
         //return (pesoideal());
         //case '3':
         //return (anobissexto());
         case 'm': 
         case 'M':
         return (main());
         break;
         case 's':
         case 'S':     
         printf("\nAté logo!\n\n\033[;34mby: @hackbartjoao\n\033[;33m");
         return 0;
         break;
         default:
           green();
           printf("Digite [M] ou [S]");
     }
    }while(1);
}







  ///////////////////////////////////////////////////////////////
 //////////////////////// int main /////////////////////////////
///////////////////////////////////////////////////////////////

int main ( void )
{

float menu(); {

system("clear");

int choice=0;
  while(choice!='5') 
  {
    red();
    printf("\n\t            MENU VERIFICADORES");
    printf("\n\t------------------------------");
    printf("\n\n\t\033[;33m1. FIBONACCI");
    printf("\n\t\033[;33m2. PESO IDEAL");
    printf("\n\t\033[;33m3. ANO BISSEXTO");
    printf("\n\t\033[;33m4. DIAS ANO");
    printf("\n\t5. SAIR");
    printf("\n\n\t\033[1;32mEscolha um número: \n\n\t");
    yellow();
    choice = getchar();
    switch(choice)
    {
    case '1': 
      system("clear");
      fibonacci();
      retry();
      return 0;
      break;
    case '2':
      system("clear");
      pesoideal();
      retry();
      return 0;
      break;
    case '3':
      system("clear");
      anobissexto();
      retry();
      return 0;
      break;
    case '4':
      system("clear");
      anosdiasnumero();
      retry();
      return 0;
      break;
    case '5':
      printf("\n\n\033[;33mAté logo!\n\n\033[;34mby: @hackbartjoao\n\033[;33m");
      break;
    default:
      red();
      printf("\n\nESCOLHA INVÁLIDA... Tente novamente\n");
    getchar();
    }
    
    (void)getchar();
  }
}




  
  return 0;
}